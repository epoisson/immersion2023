# creation grille par comprehension Python
def creeGrille(T,valeur=-1):
    plateau=[[valeur for n in range(T)] for n in range(T)]
    return plateau

# initialisation par boucle d'un objet plateau existant
def initGrille(grille,valeur=-1):
    T=len(plateau[0])
    print(T)
    for ligne in range(0,T):
        for colonne in range(0,T):
            plateau[ligne][colonne]=valeur

# initialisation du plateau pour débuter une partie
def remplirGrille(grille):
    TAILLE=len(grille)
    # remplissage des 3 lignes de 1
    debut=int (TAILLE/3)
    for i in range(debut,debut+3):
        for j in range(TAILLE):
            grille[i][j] = 1
    # remplissage des 3 colonnes de 1
    for j in range(debut,debut+3):
        for i in range(TAILLE):
            grille[i][j] = 1
    # remplissage de la case centrale à 0    
    milieu=int(TAILLE/2)
    grille[milieu][milieu] = 0

# Affichage numerique du Plateau
def afficherNumeriqueGrille(grille):
    TAILLE=len(grille)
    #entete des colonnes*/
    print("\n");
    for j in range(0,TAILLE):
        print('\t',j+1,end='')
    print("\n");
    # lignes de la grille
    for i in range(0,TAILLE):
        print(i+1,'\t',end='')
        for j in range(0,TAILLE):
            print(grille[i][j],'\t',end='')
        print("\n")
    print("\n")

# Affichage ameliore du Plateau
def afficherGrille(grille):
    TAILLE=len(grille)
    #entete des colonnes*/
    print("----------------------------------------------------------------------------\n")
    for j in range(0,TAILLE):
        print('\t',j+1,end='')
    print("\n----------------------------------------------------------------------------\n")
    # lignes de la grille
    for i in range(0,TAILLE):
        print(i+1,'|\t',end='')
        for j in range(0,TAILLE):
            if grille[i][j] == -1:
                print(".",'\t',end='')
            elif grille[i][j]==1:
                print("1",'\t',end='')
            else:
                print(" ",'\t',end='')
        print("\n")
    print("----------------------------------------------------------------------------\n")
    
# calcule le nombre de Pions dans la grille.
def nbPions(grille):
    ct = 0
    TAILLE=len(grille)
    for i in range(TAILLE):
        for j in range(TAILLE):
            if grille[i][j] == 1:
                ct=ct+1
    return ct


# renvoie si prise possible d'un pion
# il faut pour cela 
# que le pion reste sur le plateau
# un pion existant 
# ET un pion voisin à côté de lui dans la direction désirée
# ET aucun pion dans la case suivante du pion voisin
def estPriseHD(grille, x, y):
    if y>len(grille)-2:
        return False
    else:
        return ( (grille[x-1][y-1] == 1)
             and (grille[x-1][y-1+1] == 1)
             and (grille[x-1][y-1+2] == 0))

def estPriseHG(grille, x, y):
    if y<3:
        return False
    else:
        return ((grille[x-1][y-1] == 1)
            and (grille[x-1][y-1-1] == 1)
            and (grille[x-1][y-1-2] == 0))

def estPriseVH(grille, x, y):
    if x<3:
        return False
    else:
        return ((grille[x-1][y-1] == 1)
            and (grille[x-1-1][y-1] == 1)
            and (grille[x-1-2][y-1] == 0))

def estPriseVB(grille,x,y):
    if x>len(grille)-2:
        return False
    else:
        return ((grille[x-1][y-1] == 1)
            and (grille[x-1+1][y-1] == 1)
            and (grille[x-1+2][y-1] == 0))

# etudie si la prise est valide quelque soit le deplacement desiré (dep='HAUT','BAS','DROITE','GAUCHE')
# appelle les fonctions precedentes en fonction du deplacement
def estPrise(grille,x,y, dep):
    return (
               ( (dep == 'DROITE') and estPriseHD(grille,x,y))
               or ( (dep == 'GAUCHE') and estPriseHG(grille,x,y))
               or ( (dep == 'HAUT') and estPriseVH(grille,x,y))
               or ( (dep == 'BAS') and estPriseVB(grille,x,y))
           )

# les fonctions suivantes appliquent la prise et modifie le plateau en consequence
# soit deux cases vides 0 pour le pion et son voisin et mise d'un pion 1 dans la suivante  
def priseHD(grille,x, y):
    grille[x-1][y-1] = 0
    grille[x-1][y-1+1] = 0
    grille[x-1][y-1+2] = 1

def priseHG(grille,x, y):
    grille[x-1][y-1] = 0
    grille[x-1][y-1-1] = 0
    grille[x-1][y-1-2] = 1

def priseVH(grille,x, y):
    grille[x-1][y-1] = 0
    grille[x-1-1][y-1] = 0
    grille[x-1-2][y-1] = 1

def priseVB(grille,x,y):
    grille[x-1][y-1] = 0
    grille[x-1+1][y-1]  = 0
    grille[x-1+2][y-1] = 1

# renvoie le nombre de coups possibles à partir d'une case (x,y) du solitaire, c'est-à-dire le nombre de pions différents pouvant être pris à partir de cette case
def nbCoups(grille,x,y):
    nb = 0
    nb = estPriseHD(grille,x,y)+ estPriseHG(grille,x,y)+ estPriseVH(grille,x,y)+estPriseVB(grille,x,y)
    return nb

# renvoie le nombre de coups total encore possible pour la configuration du plateau donn"
def nbCoupsTotal (grille):
    nb = 0;
    TAILLE=len(grille)
    # parcours de chaque case
    for i in range(TAILLE):
        for j in range(TAILLE):
            nb += nbCoups(grille,i,j)
    return nb

# modifie le plateau en fonction de la demande de coup (xPion, yPion, dep=deplacement)
def jouerCoup(grille, x, y, dep):
    if dep == 'DROITE':
        priseHD(grille,x,y)
    elif dep == 'GAUCHE':
        priseHG(grille,x,y)
    elif dep == 'HAUT':
        priseVH(grille,x,y)
    else:
        priseVB(grille,x,y);

# demande un entier en console
def demandeEntier(message):
    while True:
        try:
            a = int(input(message))
            if a > 0:
                return a
        except ValueError:
            continue

# effectue une partie
def jouerPartie(grille):
    TAILLE=len(grille)
    # boucle tant qu'il reste des coups possibles
    while nbCoupsTotal(grille) > 0:
        afficherGrille(grille)
        print("il reste ",nbCoupsTotal(grille),"coups possibles")
        x=demandeEntier("Entrer la ligne :")
        y=demandeEntier("Entrer la colonne :")
        while (x < 1) or (x > TAILLE) or (y < 1) or (y > TAILLE) or (grille[x-1][y-1]==0) or(nbCoups(grille,x,y) < 1) :
            print("Erreur ligne ou colonne, recommencer !")
            x=demandeEntier("Entrer la ligne :")
            y=demandeEntier("Entrer la colonne :")
        print("Entrer le deplacement (HAUT, BAS, DROITE, GAUCHE) :")
        dep=input()
        while (dep!='DROITE') and (dep!='BAS') and (dep!= 'HAUT') and (dep!='BAS'):
            print( "Erreur deplacement, recommencer !")
        if estPrise(grille,x,y,dep)==False:
            print("Prise impossible ")
        while (estPrise(grille,x,y, dep)):
            jouerCoup(grille,x,y,dep)
    if nbPions(grille) == 1:
        print("c'est gagne !")
    else:
        print("c'est perdu !", "Il reste ",nbPions(grille)," pions")
    
if __name__ == '__main__':
    # Ecriture liste de liste python "à la main"
    plateau = [
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
        [ 1, 1,  1, 1, 1, 1,  1,  1,  1,], 
        [ 1, 1,  1, 1, 0, 1,  1,  1,  1,], 
        [ 1, 1,  1, 1, 1, 1,  1,  1,  1,], 
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
    ]
    
    # affichage non structuré (linéaire)
    print(plateau)
    
    # definion du plateau 
    TAILLE = 9 
    plateau=creeGrille(T=TAILLE)
    # remplissage du plateau selon le jeu du solitaire
    remplirGrille(plateau)
    # affichage structuré
    print(afficherNumeriqueGrille(plateau))
    # affichage amélioré
    print(afficherGrille(plateau))
    print("pions sur le plateau=",nbPions(plateau))

    #effectuer une partie
    plateau = [
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
        [-1, -1, -1, 0, 0, 0, -1, -1, -1], 
        [-1, -1, -1, 0, 0, 1, -1, -1, -1], 
        [ 0, 0,  1, 0, 0, 0,  0,  0,  1,], 
        [ 0, 0,  1, 0, 0, 0,  0,  0,  1,], 
        [ 0, 0,  1, 0, 1, 1,  0,  0,  1,], 
        [-1, -1, -1, 0, 0, 0, -1, -1, -1], 
        [-1, -1, -1, 0, 0, 0, -1, -1, -1], 
        [-1, -1, -1, 1, 1, 1, -1, -1, -1], 
    ]
    jouerPartie(plateau)
    