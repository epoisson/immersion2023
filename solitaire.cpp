/* Fichier TRAME à compléter */

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <array>
#include <cmath>
#include <cassert>
using namespace std;


const int TAILLE=9;
using Solitaire = int[TAILLE][TAILLE] ;

const int PION = 1;
const int VIDE = 0;
const int HORSGRILLE = -1;

const char GAUCHE = 'G';
const char DROITE = 'D';
const char HAUT = 'H';
const char BAS = 'B';

void initGrille(int s[TAILLE][TAILLE])
{
    for(int i=0; i< TAILLE; i++)
    {
        for(int j=0; j< TAILLE; j++)
        {
            s[i][j] = HORSGRILLE;
        }
    }
}

void remplirGrille(int s[TAILLE][TAILLE])
{
    // à compléter
}
    
void afficherGrille(int s[TAILLE][TAILLE])
{
    /* entete des colonnes*/
    cout << "  ";
    for(int j=0; j< TAILLE; j++)
    {
        cout << (j+1);
    }
    cout << endl;
    /* lignes de la grille */
    for(int i=0; i< TAILLE; i++)
    {
        cout << (i+1) << " ";
        for(int j=0; j< TAILLE; j++)
        {
            if (s[i][j] == HORSGRILLE)
            {
                cout << ".";
            }
            else if (s[i][j] == PION)
            {
                cout << "1";
            }
            else
            {
                cout << " ";
            }
        }
        cout << endl;
    }
}

int nbPions(int s[TAILLE][TAILLE])
{
    int ct = 0;
    // à compléter
    return ct;
}

// écrire les autres fonctions demandées ici

int main()
{
    /* DECLARATIONS */
    int s[TAILLE][TAILLE];
    int x,
        y;
    char dep;
    /* INITIALISATION */
    initGrille(s);
    remplirGrille(s);
    //afficherGrille(s);
    /* TRAITEMENT */

    //à compléter

    /* FIN */
    return EXIT_SUCCESS;
}
